package br.com.samples.paises.task;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import br.com.samples.paises.model.Country;
import br.com.samples.paises.util.NetworkUtil;


public class FetchCountryTask extends AsyncTask<URL, Void, List<Country>> {

    String TAG = "FetchCountryTask";

    public interface OnFetchListener{
        void onStartFetchCountry();
        void onEndFetchCountry(List<Country> countries);
    }

    OnFetchListener mListener;

    public FetchCountryTask(OnFetchListener mListener) {
        this.mListener = mListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mListener.onStartFetchCountry();
    }

    @Override
    protected List<Country> doInBackground(URL... urls) {
        URL url = urls[0];
        Log.d(TAG, "url utilizada: " + url.toString());
        String json = null;
        try {
            json = NetworkUtil.getResponseFromHttpUrl(url);
            Log.d(TAG, "async task retorno: " + json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<List<Country>>(){}.getType();
        List<Country> countries = new Gson().fromJson(json, listType);
        return countries;
    }

    @Override
    protected void onPostExecute(List<Country> countries) {
        super.onPostExecute(countries);
        mListener.onEndFetchCountry(countries);
    }
}
