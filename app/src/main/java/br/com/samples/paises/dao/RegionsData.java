package br.com.samples.paises.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.samples.paises.model.Region;

public class RegionsData {

    public static List<Region> getRegions(){
        List<Region> regions = new ArrayList<Region>();

        Region americas = new Region("americas", "América", "http://www.gunnebogroup.com/en/PublishingImages/About/gunnebo-worldmap-Americas-regions-distributors.png");
        Region africa = new Region("africa", "África", "http://www.stratosat.co.za/wp-content/uploads/2016/11/africa.png");
        Region asia = new Region("asia", "Ásia", "https://www.pinclipart.com/picdir/middle/203-2035284_sri-lanka-world-map-globe-world-map-map.png");
        Region europe = new Region("europe", "Europa", "https://info.watchingman.com/wp-content/uploads/2014/03/mapa-europa.png");
        Region oceania = new Region("oceania", "Oceania", "http://www.eissound.com/distributors/images/mapaoceanía.png");

        regions.add(africa);
        regions.add(americas);
        regions.add(asia);
        regions.add(europe);
        regions.add(oceania);
        return regions;
    }
}
