package br.com.samples.paises.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.samples.paises.model.FavoriteCountry;

@Dao
public interface FavoriteCountryDao {

    @Query("SELECT * FROM favoritecountry")
    List<FavoriteCountry> getFavoriteCountries();

    @Query("SELECT * FROM favoritecountry WHERE countryId LIKE :countryId")
    FavoriteCountry getFavoriteCountryById(String countryId);

    @Insert
    void insert(FavoriteCountry... countries);

    @Update
    void update(FavoriteCountry... countries);

    @Delete
    void delete(FavoriteCountry... countries);
}
