package br.com.samples.paises.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.samples.paises.R;
import br.com.samples.paises.adapter.ListImageAdapter;
import br.com.samples.paises.model.Country;
import br.com.samples.paises.model.Currency;
import br.com.samples.paises.model.FavoriteCountry;
import br.com.samples.paises.task.DatabaseTask;

public class FavoriteCountriesActivity extends AppCompatActivity implements DatabaseTask.OnQueryDatabaseListener, AdapterView.OnItemClickListener {

    ListView lvFavoriteCountries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_countries);
        setTitle("My Favorite Countries");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lvFavoriteCountries = findViewById(R.id.lv_favorite_countries);
        lvFavoriteCountries.setOnItemClickListener(this);
        new DatabaseTask.DatabaseQueryTask(this, getApplicationContext()).execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new DatabaseTask.DatabaseQueryTask(this, getApplicationContext()).execute();
    }

    private void showFavoriteList(List<FavoriteCountry> countries){
        ListImageAdapter<FavoriteCountry> adapter =  new ListImageAdapter<>(countries);
        lvFavoriteCountries.setAdapter(adapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartQueryFavorite() {

    }

    @Override
    public void onEndQueryFavorite(List<FavoriteCountry> countries) {
        if(countries != null && countries.size() > 0 ){
            showFavoriteList(countries);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FavoriteCountry f = (FavoriteCountry) parent.getAdapter().getItem(position);

        Country country = new Country();
        country.setAlpha2Code(f.getCountryId());
        country.setName(f.getName());
        country.setRegion(f.getRegion());
        country.setPopulation(Integer.valueOf(f.getPopulation()));
        List<String> strings = Arrays.asList(f.getCurrency().split(","));
        List<Currency> currencies =  new ArrayList<Currency>();
        for(String s : strings){
            Currency currency = new Currency();
            currency.setName(s);
            currencies.add(currency);
        }
        country.setCurrencies(currencies);

        Intent intent = new Intent(this, CountryDetailActivity.class);
        intent.putExtra(CountryDetailActivity.COUNTRY_KEY, country);
        startActivity(intent);

    }
}
