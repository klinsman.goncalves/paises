package br.com.samples.paises.model;

import java.io.Serializable;

public class Region extends ListViewData implements Serializable {
    private String region;
    private String name;
    private String imageUrl;

    public Region(String region, String name, String imageUrl) {
        this.region = region;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String getImageUrlListData() {
        return getImageUrl();
    }

    @Override
    public String getDescriptionListData() {
        return getName();
    }
}
