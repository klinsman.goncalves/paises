package br.com.samples.paises.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.samples.paises.R;
import br.com.samples.paises.model.Country;
import br.com.samples.paises.model.Currency;
import br.com.samples.paises.model.FavoriteCountry;
import br.com.samples.paises.model.Region;
import br.com.samples.paises.task.DatabaseTask;

public class CountryDetailActivity extends AppCompatActivity implements View.OnClickListener, DatabaseTask.OnQueryDatabaseListener {

    public static String COUNTRY_KEY = "country_key";

    ImageView ivFlag;
    TextView tvCountryName;
    TextView tvCountryRegion;
    TextView tvCountryPopulation;
    TextView tvCountryCurrency;
    TextView tvCountryLatlng;
    Button btVisit;
    Country country;

    Boolean isFavorite = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ivFlag = findViewById(R.id.iv_flag);
        tvCountryName = findViewById(R.id.tv_country_name);
        tvCountryRegion = findViewById(R.id.tv_country_region);
        tvCountryPopulation = findViewById(R.id.tv_country_population);
        tvCountryCurrency = findViewById(R.id.tv_country_currency);
        tvCountryLatlng = findViewById(R.id.tv_country_latlng);

        btVisit = findViewById(R.id.bt_visit);
        btVisit.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent.hasExtra(COUNTRY_KEY)){
            country = (Country) intent.getSerializableExtra(COUNTRY_KEY);
            setTitle(country.getName());
            setCountryData(country);
        }
    }

    private void setCountryData(Country country){
        Picasso.with(this).load(country.getImageUrlListData()).into(ivFlag);
        tvCountryName.setText(country.getName());
        tvCountryRegion.setText(country.getRegion());
        tvCountryPopulation.setText(country.getPopulation().toString());
        tvCountryCurrency.setText(country.getCurrenciesStr());
        //String location = country.getLatlng().get(0) + " - " + country.getLatlng().get(1);
        //tvCountryLatlng.setText(location);
        new DatabaseTask.DatabaseQueryTask(this, getApplicationContext()).execute(country.getAlpha2Code());
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.bt_visit){
            if(isFavorite){
                btVisit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
                Toast.makeText(this, "Country removed", Toast.LENGTH_LONG).show();
                if(country != null){
                    FavoriteCountry favoriteCountry = new FavoriteCountry(country.getAlpha2Code(),
                            country.getName(), country.getRegion(), country.getPopulation().toString()
                            , country.getCurrenciesStr(), country.getImageUrlListData());
                    new DatabaseTask.DatabaseDeleteTask(this).execute(favoriteCountry);
                }
            }else{
                btVisit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
                Toast.makeText(this, "Country added", Toast.LENGTH_LONG).show();
                if(country != null){
                    FavoriteCountry favoriteCountry = new FavoriteCountry(country.getAlpha2Code(),
                            country.getName(), country.getRegion(), country.getPopulation().toString()
                            , country.getCurrenciesStr(), country.getImageUrlListData());
                    new DatabaseTask.DatabaseInsertTask(this).execute(favoriteCountry);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartQueryFavorite() {

    }

    @Override
    public void onEndQueryFavorite(List<FavoriteCountry> countries) {
        if(countries != null && countries.size() > 0){
            btVisit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
            isFavorite = true;
        }else{
            isFavorite = false;
        }
    }
}
