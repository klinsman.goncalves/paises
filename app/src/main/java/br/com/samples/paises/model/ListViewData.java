package br.com.samples.paises.model;

import java.io.Serializable;

public abstract class ListViewData implements Serializable {
    public abstract String getImageUrlListData();
    public abstract String getDescriptionListData();
}
