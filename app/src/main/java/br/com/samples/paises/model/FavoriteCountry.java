package br.com.samples.paises.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity
public class FavoriteCountry extends ListViewData implements Serializable {

    @PrimaryKey
    @NonNull
    private String countryId;
    private String name;
    private String region;
    private String population;
    private String currency;
    private String imageUrl;

    public FavoriteCountry() {

    }

    @Ignore
    public FavoriteCountry(String countryId, String name, String region, String population, String currency, String imageUrl) {
        this.countryId = countryId;
        this.name = name;
        this.region = region;
        this.population = population;
        this.currency = currency;
        this.imageUrl = imageUrl;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String getImageUrlListData() {
        return getImageUrl();
    }

    @Override
    public String getDescriptionListData() {
        return getName();
    }
}
