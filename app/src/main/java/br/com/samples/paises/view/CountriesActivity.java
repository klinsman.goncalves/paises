package br.com.samples.paises.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.net.URL;
import java.util.List;

import br.com.samples.paises.R;
import br.com.samples.paises.adapter.ListImageAdapter;
import br.com.samples.paises.model.Country;
import br.com.samples.paises.model.Region;
import br.com.samples.paises.task.FetchCountryTask;
import br.com.samples.paises.util.NetworkUtil;

public class CountriesActivity extends AppCompatActivity implements FetchCountryTask.OnFetchListener, AdapterView.OnItemClickListener {

    public static final String REGION_KEY = "region_key";

    ListView lvCountries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lvCountries = findViewById(R.id.lv_countries);
        lvCountries.setOnItemClickListener(this);

        Intent intent = getIntent();

        if (intent.hasExtra(REGION_KEY)){
            Region region = (Region) intent.getSerializableExtra(REGION_KEY);
            setTitle(region.getName());
            showCountryList(region);
        }
    }

    private void showCountryList(Region region){
        FetchCountryTask task = new FetchCountryTask(this);
        URL url = NetworkUtil.buildUrl(region.getRegion());
        task.execute(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartFetchCountry() {

    }

    @Override
    public void onEndFetchCountry(List<Country> countries) {
        ListImageAdapter<Country> countryAdapter = new ListImageAdapter<Country>(countries);
        lvCountries.setAdapter(countryAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Country country = (Country) parent.getAdapter().getItem(position);
        openCountryDetail(country);
    }

    public void openCountryDetail(Country country){
        Intent intent = new Intent(this, CountryDetailActivity.class);
        intent.putExtra(CountryDetailActivity.COUNTRY_KEY, country);
        startActivity(intent);
    }
}
