package br.com.samples.paises.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.samples.paises.dao.CountryDatabase;
import br.com.samples.paises.model.FavoriteCountry;

public class DatabaseTask {
    String TAG = "DatabaseTask";

    public interface OnQueryDatabaseListener{
        void onStartQueryFavorite();
        void onEndQueryFavorite(List<FavoriteCountry> countries);
    }

    public static class DatabaseInsertTask extends AsyncTask<FavoriteCountry, Void, Boolean>{
        final Context mContext;

        public DatabaseInsertTask(Context context) {
            mContext = context;
        }

        @Override
        protected Boolean doInBackground(FavoriteCountry... countries) {
            try {
                CountryDatabase.getInstance(mContext).getDao().insert(countries);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;

        }
    }

    public static class DatabaseDeleteTask extends AsyncTask<FavoriteCountry, Void, Boolean>{
        final Context mContext;

        public DatabaseDeleteTask(Context context) {
            mContext = context;
        }

        @Override
        protected Boolean doInBackground(FavoriteCountry... countries) {
            try {
                CountryDatabase.getInstance(mContext).getDao().delete(countries);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;

        }
    }


    public static class DatabaseQueryTask extends AsyncTask<String, Void, List<FavoriteCountry>>{

        final OnQueryDatabaseListener mListener;
        final Context mContext;

        public DatabaseQueryTask(OnQueryDatabaseListener listener, Context context) {
            mListener = listener;
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.onStartQueryFavorite();
        }

        @Override
        protected List<FavoriteCountry> doInBackground(String... strings) {
            List<FavoriteCountry> favoriteCountries = null;
            try {
                //if there is no query parameter return all favorite countries
                if(strings.length == 0) {
                    favoriteCountries = CountryDatabase.getInstance(mContext).getDao().getFavoriteCountries();
                    return favoriteCountries;
                }

                //else, use the first string as parameter
                String countryId = strings[0];
                FavoriteCountry favorite = CountryDatabase.getInstance(mContext).getDao().getFavoriteCountryById(countryId);

                //if does not find the favorite return null
                if(favorite == null){
                    return null;
                }

                //else return the favorite country by selected id
                favoriteCountries = new ArrayList<FavoriteCountry>();
                favoriteCountries.add(favorite);

            } catch (Exception e) {
                e.printStackTrace();;
            }
            return favoriteCountries;
        }

        @Override
        protected void onPostExecute(List<FavoriteCountry> favoriteCountries) {
            mListener.onEndQueryFavorite(favoriteCountries);
        }
    }

}
