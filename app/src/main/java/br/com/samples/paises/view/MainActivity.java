package br.com.samples.paises.view;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import br.com.samples.paises.R;
import br.com.samples.paises.adapter.ListImageAdapter;
import br.com.samples.paises.dao.RegionsData;
import br.com.samples.paises.model.Region;
import br.com.samples.paises.view.CountriesActivity;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listRegions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listRegions = findViewById(R.id.lv_regions);
        listRegions.setOnItemClickListener(this);

        List<Region> regions = RegionsData.getRegions();
        ListImageAdapter<Region> adapter = new ListImageAdapter<Region>(regions);
        listRegions.setAdapter(adapter);

        Log.d("AAAB", this.getPackageName());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Region region = (Region) parent.getAdapter().getItem(position);
        openCountryList(region);
    }

    private void startActivityFavorite(){
        Intent intent =  new Intent(this, FavoriteCountriesActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.menu_place_to_visit:
                startActivityFavorite();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void openCountryList(Region region){
        Intent intent = new Intent(this, CountriesActivity.class);
        intent.putExtra(CountriesActivity.REGION_KEY, region);
        startActivity(intent);
    }
}
