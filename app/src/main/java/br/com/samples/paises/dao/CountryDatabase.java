package br.com.samples.paises.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import br.com.samples.paises.model.FavoriteCountry;

@Database(entities = {FavoriteCountry.class}, version = 1)
public abstract class CountryDatabase extends RoomDatabase {

    private static final String DB_NAME = "favoritecountries.db";
    private static volatile CountryDatabase instance;

    public static CountryDatabase getInstance(Context context){
        if(instance == null){
            instance = create(context);
        }
        return instance;
    }

    private static CountryDatabase create(Context context){
        return Room.databaseBuilder(context, CountryDatabase.class, DB_NAME).build();
    }

    public abstract FavoriteCountryDao getDao();
}
